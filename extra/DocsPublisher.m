%% |DocsPublisher|: Automatically generate documentation using MATLAB Publish
% Call me from a pre-commit git hook!
% See README.md for guidance and resources.
%


%% Summary
% |DocsPublisher("auto")| traverses the project directory tree recursively
% in search of .m files to generate .html documentation from.
%
% |DocsPublisher("index")| looks up the file |extra/index.txt| containing
% the names of the .m files to generate .html documentation from.
% It also overwrites the file |extra/index.txt| with the relative paths of
% the generated documents for git to automatically stage the documentation.
% This is the intended mode to call when using DocsPublisher from a
% pre-commit hook.
%
% |DocsPublisher("manual", [mFile1, ..., mFileN])| generates .html
% documentation from the manually specified .m files.


%% Requirements
% * MATLAB(R) R2016+
% * Directory tree:
%%
%   MATLAB_PROJECT_ROOT
%       extra
%           DocsPublisher.m
%       ...
%%
%


function DocsPublisher (mode, varargin)

% splash print
fprintf("Initialising DocsPublisher...\n");

% change the MATLAB current folder to the root of the current project
cd(fileparts(mfilename("fullpath")));
cd("../");
% add all subfolders to the MATLABpath
currentFolder = pwd();
addpath(genpath(currentFolder));

% set MATLAB Publish options
optionsDefault.outputDir = strcat(currentFolder, "/docs/html/");
optionsDefault.format = "html";
optionsDefault.showCode = false;
optionsDefault.evalCode = false;

switch mode
    case "auto"
        % fetch all .m files at any depth under the MATLAB current folder
        % and extract their paths and names
        fprintf("Fetching all .m files in project...\n");
        dotMData = dir("**/*.m");
        dotMPaths = {dotMData(:).folder};
        dotMNames = {dotMData(:).name};
    case "index"
        % read the .m files from the index file
        % and extract their paths and names
        fprintf("Reading .m files from index.txt...\n");
        indexData = readlines("extra/index.txt", "WhitespaceRule", "trim", "EmptyLineRule", "skip");
        [indexPaths, indexNames, indexExts] = fileparts(indexData);
        indexFullNames = strcat(indexNames, indexExts);
        dotMPaths = indexPaths(strcmpi(indexExts, ".m"));
        dotMNames = indexFullNames(strcmpi(indexExts, ".m"));
    case "manual"
        % read the .m files from the input vector
        % and extract their paths and names
        fprintf("Reading .m files from input...\n");
        [inputPaths, inputNames, inputExts] = fileparts(varargin{1}.');
        inputFullNames = strcat(inputNames, inputExts);
        dotMPaths = inputPaths(strcmpi(inputExts, ".m"));
        dotMNames = inputFullNames(strcmpi(inputExts, ".m"));
    otherwise
        error("Unrecognised mode! Valid modes are: 'auto', 'index', 'manual'.");
end

% open index of new publishes
if strcmpi(mode, "index")
    fID = fopen("extra/index.txt", "w");
end

% generate all the docs!
% and add them to the index
l = length(dotMNames);
fprintf("Generating %d docs", l);
for idx = 1 : l
    % get name and relative path of the file
    dotMFile = dotMNames(idx);
    dotMRelativePath = erase(dotMPaths(idx), currentFolder);
    % overwrite the options to mirror the project directory tree in docs
    options = optionsDefault;
    options.outputDir = strcat(options.outputDir, dotMRelativePath, "/");
    % generate the document and write it to the file
    pub = publish(string(dotMFile), options);
    if strcmpi(mode, "index")
        printf(fID, "%s ", erase(pub, strcat(currentFolder, "/")));
    end
    % print success counter to stdout
    fprintf(".");
end

% close index of new docs
if strcmpi(mode, "index")
    fclose(fID);
end
fprintf("\nDone!\n");

end
